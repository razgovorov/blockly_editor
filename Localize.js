import {objHasOwnProperty} from "bubot-helpers/BaseHelper"
import axios from "axios"
import Blockly from "blockly";

export default {
    data() {
        return {
            lang: '',
            defaultLang: 'en',
            locales: {}
        }
    },
    methods: {
        setLocale: async function (lang) {
            if (!lang) {
                lang = navigator.language.substr(0, 2).toLowerCase()
            }
            this.lang = lang
            axios.defaults.headers.common['Accept-Language'] = lang
            await this.loadLocale(lang)
            return lang
        },
        loadLocale: async function (lang) {
            if (!objHasOwnProperty(this.locales, lang)) {
                console.log(`locale ${lang} not found, set ${this.defaultLang}`)
                lang = this.defaultLang
            }
            let locale_files = this.locales[lang]
            let i, len
            if (!locale_files) {
                return
            }
            for (i = 0, len = locale_files.length; i < len; ++i) {
                // let locale = await import(/* webpackChunkName: "lang-[request]" */ `${locale_files[i]}`)
                let locale = locale_files[i]
                let key
                for (key in locale) {
                    if (Object.prototype.hasOwnProperty.call(locale, key)) {
                        Blockly.Msg[key.toUpperCase()] = locale[key]  // к сожалению блокли воспринимает только так
                    }
                }
            }
        }
    }
}