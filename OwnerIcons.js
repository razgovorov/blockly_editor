export default {
    'global': 'mdiWeb',
    'system': 'mdiTree',
    'base': 'mdiLeaf',
    'standard': 'mdiLeaf',
    'custom': 'mdiAccountGroup',
    'personal': 'mdiAccountCowboyHat',
}