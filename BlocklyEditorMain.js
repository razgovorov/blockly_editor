import ExtException from "bubot-helpers/ExtException";
import UrlParam from "bubot-helpers/UrlParam";
import Blockly from "blockly";
import {
    mdiAlertCircle, mdiAlertCircleOutline,
    mdiChevronDoubleLeft,
    mdiChevronDoubleRight, mdiClose, mdiCog, mdiCompare,
    mdiContentSave, mdiDebugStepInto, mdiDebugStepOver,
    mdiDownload, mdiEraserVariant,
    mdiExportVariant, mdiFunction,
    mdiFolderOpen, mdiMathLog, mdiMenuDown, mdiPlay, mdiSpider, mdiStepForward, mdiStop
} from "@mdi/js";
import {connectors} from "@/BlocklyEditor/Connector/connectors";
import {hideChanges} from "./BlocklyEditorWorkspace";

export default {
    data() {
        return {
            svg: {
                mdiExportVariant,
                mdiDownload,
                mdiFolderOpen,
                mdiContentSave,
                mdiChevronDoubleLeft,
                mdiChevronDoubleRight,
                mdiMathLog,
                mdiStepForward,
                mdiSpider,
                mdiPlay,
                mdiEraserVariant,
                mdiClose,
                mdiStop,
                mdiCog,
                mdiDebugStepInto,
                mdiDebugStepOver,
                mdiAlertCircle,
                mdiAlertCircleOutline,
                mdiMenuDown,
                mdiCompare, mdiFunction
            },
            toolbox: ['standard'],
            code: '',
            appName: '',
            options: undefined,
            requestedWorkspaces: [],
            debugPanelVisible: false,
            debugPanelTab: 0,
            classBlockly: 'blockly-full-width',
            stepByStep: false,
            debugProcessed: false,
            protocolId: '',
            logVisible: false,
            iniListVisible: false,
            settingsVisible: false,
            functionListVisible: false,
            settings: {
                Algorithm: '',
                AlgorithmFileType: 'json',
                Service: 'CurrentHost',
                Connector: 'CurrentHost',
                Command: 'CurrentHost',
                Endpoint: '',
                // ext_sys_version: '',
            },
            settingsDefaultValue: {
                Algorithm: '',
                AlgorithmFileType: 'json',
                Service: 'CurrentHost',
                Connector: 'CurrentHost',
                Command: 'CurrentHost',
                Endpoint: '',
                // ext_sys_version: ''
            },
            connectors,
            log: [],
            status: null,
            result: null,
            urlParam: {},
            service: null,
            connector: null,
            commandConnector: null,
            blocklyPromptShow: false,
            blocklyPromptMsg: '',
            blocklyPromptDefault: null,
            blocklyPromptCallback: null,
        }
    },
    mounted() {
    },
    watch: {
        settings: async function () {
            await this.initConnectors()
            this.updateUrlParam()
            if (this.currentAlgorithm !== this.settings.Algorithm) {
                console.log('watch load algorithm', this.currentAlgorithm)
                this.load()
            }

        },
    },
    async beforeMount() {
        console.log('mounted editor')
        this.urlParam = new UrlParam()
        this.setLocale()
        await this.initSettingsFromUrl()
        this.currentAlgorithm = this.settings.Algorithm  // костыль, т.к. алгоритм будет грузиться по завершению загрузки редактора
        // await this.initConnectors()
        let self = this
        Blockly.dialog.setPrompt(function (msg, defaultValue, callback) {
            self.blocklyPromptShow = true
            self.blocklyPromptMsg = msg
            self.blocklyPromptDefault = defaultValue
            self.blocklyPromptCallback = callback
        })
    },
    methods: {
        initConnector: function (name, log, appName) {
            let Connector = this.connectors[name]
            if (!Connector) {
                throw new Error(`Connector ${name} not defined`)
            }
            try {
                return new Connector(log, appName)
            } catch (err) {
                throw new Error(`Bad connector ${name}: ${err}`)
            }
        },
        initConnectors: async function () {

            let currentExecutor = this.service ? this.service.constructor.name : undefined

            let needInit = false
            if (currentExecutor !== this.settings.Service) {
                this.service = this.initConnector(this.settings.Service, this.log, this.appName)
                needInit = true
            }
            if (this.settings.Service === this.settings.Connector) {
                this.connector = this.service
            } else {
                this.connector = this.initConnector(this.settings.Connector, this.log, this.appName)
            }
            if (this.settings.Connector === this.settings.Command) {
                this.commandConnector = this.connector
            } else {
                this.commandConnector = this.initConnector(this.settings.Command, this.log, this.appName)
            }
            needInit = true
            if (!needInit) {
                return
            }
            try {
                let resp = await this.service.init(Object.assign({toolbox: this.toolbox}, this.settings));
                this.options = resp.data
            } catch (err) {
                this.status = 'error'
                this.result = new ExtException({message: 'Failed init editor', parent: err})
            }
        },
        updateAlgorithmName: function (value) {
            this.settings.Algorithm = value
            this.currentAlgorithm = this.settings.Algorithm
            this.updateUrlParam()
        },
        updateUrlParam: function () {
            let param = {}
            for (let key in this.settings) {
                if (Object.prototype.hasOwnProperty.call(this.settings, key) && this.settings[key]) {
                    param[key] = this.settings[key]
                }
            }
            this.urlParam.param = param
            this.urlParam.updateUrl()
        },
        getAppNameFromUrl: function () {
            let _path = window.location.pathname.split('/')
            let first = _path.indexOf('ui')
            if (first < 1)
                throw new Error('Url not bubot app')
            return _path[first - 1]
        },
        initSettingsFromUrl: async function () {
            this.appName = this.getAppNameFromUrl()
            let newSettings = {}
            for (let key in this.settings) {
                if (Object.prototype.hasOwnProperty.call(this.settings, key)) {
                    newSettings[key] = this.urlParam.get(key, this.settingsDefaultValue[key])
                }
            }
            if (!Object.prototype.hasOwnProperty.call(this.connectors, this.settings.Connector)) {
                alert(`connector not found ${this.settings.Connector}`)
            }
            if (!Object.prototype.hasOwnProperty.call(this.connectors, this.settings.Command)) {
                alert(`command connector not found: ${this.settings.Command}`)
            }
            this.settings = newSettings
        },
        getBlockly: function () {
            return this.$refs["blockly1"]
        },
        getWorkSpace: function () {
            return this.getBlockly().workspace
        },
        showCode: function () {
            // this.code = BlocklyJS.workspaceToCode(this.$refs["blockly1"].workspace);
        },
        clearLog: function () {
            this.log.splice(0, this.log.length)
        },
        _checkUpdateIni: async function (resp) {
            if (resp['status'] === 'error' && resp['result'] && resp['result']['__name__'] === 'WorkspaceNotFound') {
                let workspace_name = resp['result']['detail']
                if (this.requestedWorkspaces.indexOf(workspace_name) < 0) { // такую инишку ещё не просили
                    this.requestedWorkspaces.push(workspace_name)
                    let workspaceXml = await this.request_workspace(workspace_name)
                    await this.updateIni(workspace_name, workspaceXml, this.connector)
                    return true
                }
            }
            return false
        },
        highlightBlock: async function (workspace, blockId) {
            workspace.highlightBlock(blockId)
            let currentBlockNode = this.$el.querySelector('.currentBlock')
            if (currentBlockNode) {
                currentBlockNode.classList.remove('currentBlock')
            }
            currentBlockNode = this.$el.querySelector(`[data-id="${blockId}"]`)
            if (currentBlockNode) {
                currentBlockNode.classList.add('currentBlock')
            }
        },
        showCurrentBlock: async function (workspace, currentBlock) {
            if (!currentBlock) {
                return
            }
            if (this.context.current_workspace && this.settings.Algorithm !== this.context.current_workspace) {
                this.settings.Algorithm = this.context.current_workspace
                await this.load()
            }
            workspace.centerOnBlock(currentBlock)
        },
        updateIni: async function (workspace_name, workspace_data, connector) {
            if (!connector) {
                connector = this.commandConnector
            }
            try {
                await connector.commands([{
                    name: 'algorithm_update',
                    params: {
                        name: workspace_name,
                        value: workspace_data
                    }
                }])
            } catch (e) {
                alert(e)
            }
        },
        debugPanelShow() {
            this.debugPanelVisible = !this.debugPanelVisible
            if (!this.debugPanelVisible){
                hideChanges(this.getBlockly())
            }
        },
        onChangeSettings(settings) {
            this.settingsVisible = false
            this.settings = settings
            this.initConnectors()
        },
        onBlocklyPromptComplete() {
            this.blocklyPromptCallback(this.blocklyPromptDefault)
            this.blocklyPromptShow = false
        },

        onWorkspaceEvent: function (event) {
            // console.log(`${event.type} ${event.element} ${event.newValue}`)
            switch (event.type) {
                case 'mounted':
                    // console.log('LOAD')
                    this.load()
                    break
                case 'selected':
                    this.selectedBlock = event.newElementId
                    break
                case '"viewport_change"':
                    if (event.scale === 1 && this.currentBlock) {
                        this.getWorkSpace().centerOnBlock(this.currentBlock)
                    }
                    break
            }
        },
        findBlockNodeById(blockId) {
            return this.$el.querySelector(`[data-id="${blockId}"]`)
        }
    }
}