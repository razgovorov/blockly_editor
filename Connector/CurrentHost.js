import axios from "axios";
import ExtException from "bubot-helpers/ExtException";


export default class CurrentHost {
    constructor(log, appName) {
        this._log = log || []
        this.appName = appName
        this.path = 'Algorithm/'
    }

    async init(data) {
        return await post(`/${this.appName}/api/${this.path}blockly_editor_init`, data)
    }

    async commands(data) {
        let res = await post(`/${this.appName}/api/${this.path}blockly_execute_commands`, data)
        // console.log(res.data)
        return res.data
    }

    async run(data, params) {
        try {
            // if (!data['operation']) {
            //     return {'status': 'error', 'result': {message: 'Operation not defined'}}
            // }
            let res = await post(`/${this.appName}/api/${this.path}blockly_operation_run`, data, {params})
            return res.data
        } catch (err) {
            return {status: 'error', result: err}
        }
    }

    async algorithmList(params) {
        let resp = await get(`/${this.appName}/api/${this.path}blockly_algorithm_list`, {params})
        return resp.data.rows
    }

    async load(params) {
        return await get(`/${this.appName}/api/${this.path}blockly_algorithm_read`, {params})
    }

    async workspaceRead(ConnectionId, Algorithm, AlgorithmFileType, AlgorithmOwn ) {
        return await get(`/${this.appName}/api/${this.path}blockly_algorithm_read`, {
            params: {
                ConnectionId,
                Algorithm,
                AlgorithmFileType,
                AlgorithmOwn
            }
        })
    }


    async save(data, params) {
        return await post(`/${this.appName}/api/${this.path}blockly_algorithm_update`, data, {params})
    }

    async compare(data, params) {
        return await post(`/${this.appName}/api/${this.path}blockly_algorithm_compare`, data, {params})
    }

    async connectionList(params) {
        let resp = await axios.get(`/${this.appName}/api/${this.path}blockly_connection_list`, {params})
        return resp.data
    }

    log() {
    }

}

export async function post(url, data, config) {
    try {
        let resp = await axios.post(url, data, config)
        if (resp.status === 200) {
            return resp
        } else {
            processError(resp)
        }
    } catch (err) {
        processError(err.response)
    }
}

export async function get(url, config) {
    try {
        let resp = await axios.get(url, config)
        if (resp.status === 200) {
            return resp
        } else {
            processError(resp)
        }
    } catch (err) {
        if (err.response) {
            processError(err.response)
        } else {
            throw(new ExtException({
                message: 'Ошибка отправки запроса',
                detail: `${url} - ${err.message}`,
                dump: config
            }))
        }
    }
}


function processError(response) {

    let contentType = undefined
    if (response.headers) {
        contentType = response.headers['content-type']
    }
    if (contentType && contentType.toLowerCase().indexOf('application/json') >= 0) {
        throw new ExtException(response.data)
    } else {
        throw new ExtException({
            message: `${response.status} ${response.statusText}`,
            detail: response.data
        })
    }
}
