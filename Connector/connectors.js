import Connector1C from "./1C/Connector1C";
import ConnectorSAP from "./SAP/ConnectorSap";
import ConnectorCurrentHost from "./CurrentHost";
import ConnectorLocalXml from "./LocalXml";



export const connectors = {
    '1C': Connector1C,
    'SAP': ConnectorSAP,
    'CurrentHost': ConnectorCurrentHost,
    'LocalXml': ConnectorLocalXml,
}
