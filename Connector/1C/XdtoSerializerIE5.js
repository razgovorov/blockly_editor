// import {date as format} from 'Types/formatter';
import {objHasOwnProperty} from "bubot-helpers/BaseHelper"
import ExtException from "bubot-helpers/ExtException";

// const xmlNodes = ['ValueTable', 'Value', 'Array'];
const xsiTypes = {
    'DocumentObject': 'doc',
    'Structure': 'structure',
    'ValueTable': 'table',
    'Array': 'array',
    'Null': 'null',
    'Map': 'map',
    'string': 'str',
    'decimal': 'num',
    'boolean': 'boolean',
    'dateTime': 'str'
}

const jsonNodes = {
    array: 'array',
    object: 'map',
    null: 'null',
    number: 'num',
    boolean: 'boolean',
    string: 'str'
};
// const amp = new RegExp("&", 'g');

export default class XdtoSerializer {

    constructor() {
        this.xmlDoc = null;
        this.dp1NS = {};
    }

    decode(xmlStr) {
        try {
            if (!xmlStr) {
                return;
            }
            const xml = this.loads(xmlStr);
            // if (xml.childNodes[0].getAttribute('xmlns:xsi') !== "http://www.w3.org/2001/XMLSchema-instance") {
            //     throw new Error('Not supported xmlns:xsi')
            // }
            let result = this.decodeNode(xml.childNodes[0], undefined, undefined, '');
            return result;
        } catch (err) {
            throw new ExtException({
                parent: err,
                action: 'XdtoSerializer.decode',
                // dump: {xml: xml_str}
            })

        }
    }

    decodeNode(node, parent, name, path) {
        // const xmlNs = node.getAttribute('xmlns');
        let xsiType = node.getAttribute('xsi:type');
        if (!xsiType) {
            if (!path) {
                xsiType = node.nodeName; // в корневом нет типа, берем имя тега
            } else if (node.getAttribute('xsi:nil')) {
                return null
            }
        }

        let nodeType;
        if (xsiType) {
            xsiType = xsiType.split(':');
            xsiType = xsiType[xsiType.length - 1];
            xsiType = xsiType.split('.');
            xsiType = xsiType[0];
            if (!objHasOwnProperty(xsiTypes, xsiType)) {
                if (xsiType.substr(-3) === 'Ref') {
                    nodeType = 'ref'
                } else {

                    throw new ExtException({
                        message: 'decode error',
                        detail: `Not supported xsi:type ${xsiType} ${path}`,
                        action: 'XdtoSerializer.decodeNode'
                    });
                }
            } else {
                nodeType = xsiTypes[xsiType];
            }
        } else {
            if (objHasOwnProperty(xsiTypes, node.nodeName)) {
                nodeType = xsiTypes[node.nodeName]
            } else {
                nodeType = 'simple_node';
            }
        }
        // throw new Error("Not supported node: " + node.nodeName + ", path:" + path + ", content:" + node.textContent + ",");
        const decoder = 'decode_' + nodeType
        if (decoder in this) {
            return this[decoder](node, parent, name, path + '.' + (xsiType ? xsiType : node.nodeName));
        } else
            throw new ExtException({
                action: 'XdtoSerializer.decodeNode'
            });
    }

    decode_structure(node, parent, name, path) {
        let result = {};
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                let prop = _nodes[i];
                try {
                    if (prop.nodeName === 'Property') {
                        let propName = prop.getAttribute('name');
                        result[propName] = this.decodeNode(prop.childNodes[0], result, propName, path);
                    } else {
                        throw new ExtException({message: `'not support node name in structure ${prop.nodeName} ${path}`});
                    }
                } catch (err) {
                    throw new ExtException({
                        parent: err,
                        action: 'XdtoSerializer.decode_structure',
                        dump: {
                            path: `${path}.${prop.nodeName}(${prop.getAttribute('name')}`,
                        }
                    });
                }
            }
        }
        return result;
    }

    decode_array(node, parent, name, path) {
        if (!node.childNodes.length) {
            return []
        }
        let result = [];
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                let item = _nodes[i];
                result.push(this.decodeNode(item, parent, name, `${path}.${i}`))
            }
        }
        return result
    }

    decode_map(node, parent, name, path) {
        let result = {};
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                let pair = _nodes[i];
                try {
                    let key = this.decodeNode(pair.childNodes[0], result, 'pairKey', `${path}.key${i}`);
                    result[key] = this.decodeNode(pair.childNodes[1], result, 'pairValue', `${path}.value${i}`);
                } catch (err) {
                    throw new ExtException({
                        parent: err,
                        action: 'XdtoSerializer.decode_map',
                        dump: {
                            path: `${path}.${i}`,
                        }
                    });
                }
            }
        }
        return result;
    }

    decode_table(node, parent, name, path) {
        let result = [];
        let index = [];
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                let item = _nodes[i];
                try {
                    if (item.nodeName === 'row') {
                        result.push(this.decode_table_row(_nodes[i], result, index, `${path}.${i}`));
                    } else if (item.nodeName === 'column') {
                        index.push(this.decode_simple_node(_nodes[i], undefined, undefined, path))
                    } else {
                        throw new ExtException({message: `not support node name in table ${item.nodeName}`});
                    }
                } catch (err) {
                    throw new ExtException({
                        parent: err,
                        action: 'XdtoSerializer.decode_table',
                        dump: {
                            path: `${path}.${i}`,
                        }
                    });
                }
            }
        }
        return result;
    }

    decode_simple_node(node, parent, name, path) {
        // let result = parent ? parent : {};
        const _nodes = node.childNodes;
        let _result;

        if (_nodes.length && _nodes[0].nodeType != 3) { // != Node.TEXT_NODE
            _result = {}
            for (let i = 0; i < _nodes.length; i++) {
                let nodeName = _nodes[i].nodeName
                nodeName = nodeName.split(':');
                nodeName = nodeName[nodeName.length - 1];
                let value = this.decodeNode(_nodes[i], undefined, nodeName, path);
                if (objHasOwnProperty(_result, nodeName)) {
                    if (!(_result[nodeName] instanceof Array)) {
                        _result[nodeName] = [_result[nodeName]];
                    }
                    _result[nodeName].push(value);
                } else {
                    _result[nodeName] = value
                }
            }
        } else {
            _result = node.textContent;
        }
        // if (parent && name)
        // const nodeName = name ? name : node.nodeName
        return _result;
    }

    decode_table_row(node, parent, index, path) {
        let result = {};
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                try {
                    let _nodeName = index[i]['Name'];
                    result[_nodeName] = this.decodeNode(_nodes[i], result, _nodeName, `${path}.${_nodeName}`);
                } catch (err) {
                    throw new ExtException({parent: err});
                }
            }
        }
        return result;

    }

    decode_str(node) {
        return node.textContent;
    }

    decode_null() {
        return null;
    }

    decode_doc(node, parent, name, path) {
        let result = this.decode_simple_node(node, parent, name, path)
        let xsiType = node.getAttribute('xsi:type');
        xsiType = xsiType.split(':');
        xsiType = xsiType[xsiType.length - 1];
        xsiType = xsiType.split('.');
        result['ИдИС'] = result['Ref']
        result['ИмяИС'] = xsiType[1]
        result['ТипИС'] = 'Документы'
        return result
    }

    decode_ref(node, parent, name) {
        // let xsiType = node.getAttribute('xsi:type').split(':')[1];
        let objType = node.getAttribute('xsi:type').split(':')[1].split('.');
        if (name === 'uid') {
            parent['uid.xsi:ИмяИС'] = objType[1];
            parent['uid.xsi:ТипИС'] = objType[0];
            return node.textContent;
        } else {

            return {
                'ИдИС': node.textContent,
                'ИмяИС': objType[1],
                'ТипИС': objType[0]
            }
        }
    }


    decode_num(node) {
        return Number(node.textContent);
    }

    decode_boolean(node) {
        return !!node.textContent;
    }

    encode(data) {
        if (data === undefined) {
            return ''
        }
        // const xml = this.loads('<?xml version="1.0" encoding="UTF-8"?>');
        this.dp1NS = {};
        this.xmlDoc = document.implementation.createDocument('', '', null);
        try {
            const root = this.encodeNode(data, undefined, undefined, '', 1);
            // root.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            // root.setAttributeNS('http://www.w3.org/2000/xmlns/','xmlns:xs', 'http://www.w3.org/2001/XMLSchema');
            //root.setAttribute('xmlns', 'http://v8.1c.ru/8.1/data/core');
            let xmlString = new XMLSerializer().serializeToString(root);
            // 1C нужен namespace xs для контроля значений, добавить его другими способами кроме как заменой строки одинаково для всех браузеров не вышло.
            let newNS = "xmlns=\"http://v8.1c.ru/8.1/data/core\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
            for (let level in this.dp1NS) {
                if (objHasOwnProperty(this.dp1NS, level))
                    newNS += ` xmlns:d${level}p1="http://v8.1c.ru/8.1/data/enterprise/current-config"`
            }
            let result = xmlString.replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", newNS);
            return result;
        } catch (err) {
            throw new ExtException({parent: err, action: 'XdtoSerializer.encode', dump: {data}})
        }
    }

    encodeNode(node, name, parent, path, level) {
        try {
            let node_type = typeof node;
            if (node_type === 'object') {
                if (node === null) {
                    node_type = 'null';
                } else {
                    if (!(node instanceof Object)) {
                        return
                    }
                    if (node instanceof Array) {
                        node_type = 'array';
                    } else if (node instanceof Date) {
                        node_type = 'string';
                        // node = node.toISOString();
                        // node = format(node, 'YYYY-MM-DDTHH:mm:ss.sss');
                    }
                }
            }
            if (!objHasOwnProperty(jsonNodes, node_type)) {
                return;
            }

            node_type = jsonNodes[node_type];
            let handler = 'encode_' + node_type
            if (!(this[handler] instanceof Function)) {
                throw new ExtException({
                    message: 'encode error',
                    detail: `Not supported node "${node_type}", path: ${path}`,
                    action: 'XdtoSerializer.encodeNode'
                });
            }
            return this[handler](node, name, parent, path + '.' + name ? name : node_type[0], level);
        } catch (err) {
            throw new ExtException({parent: err, action: 'encodeNode'})
        }
    }

// encode_array(node, name, parent, path) {
//     if (!node) {
//         return;
//     }
//     for (let i = 0; i < node.length; i++) {
//         this.encodeNode(node[i], null, parent, path);
//     }
// }
    is_ref(node) {
        return objHasOwnProperty(node, 'ТипИС')
            && objHasOwnProperty(node, 'ИмяИС')
            && objHasOwnProperty(node, 'ИдИС')
    }

    encode_object(node, name, parent, path, level) {
        let elem = ''
        if (this.is_ref(node)) { // объект содержащий ссылку
            return this.encode_ref(node, name, parent, path, level);
        } else {
            if (!parent) { // корень
                name = 'Structure'
                // elem = this.xmlDoc.createElement('Structure');
                elem += `<${name} xsi:type="Structure">`
                elem += '<Structure xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'
                elem += 'xsi:type="Structure">'
            } else {
                elem += `<${name} xsi:type="Structure">`
            }
            elem += `<${name} xsi:type="Structure">`
            for (const key in node) {
                if (objHasOwnProperty(node, key)) {
                    let prop= `<Property name=${key}>`
                    prop += this.encodeNode(node[key], 'Value', prop, path, level + 2);
                    prop += '</Property>'
                    elem += prop
                }
            }
            elem += `</${name}>`
        }
        return elem;
    }

    encode_map(node, name, parent, path, level) {
        let elem
        if (this.is_ref(node)) { // объект содержащий ссылку
            return this.encode_ref(node, name, parent, path, level);
        } else {
            if (!parent) { // корень
                elem = this.xmlDoc.createElement('Map');
                elem.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
                elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'Map')
            } else {
                elem = this.xmlDoc.createElement(name);
                elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'Map')
                parent.appendChild(elem);
            }
            for (const key in node) {
                if (objHasOwnProperty(node, key)) {
                    let pair = this.xmlDoc.createElement("pair");
                    elem.appendChild(pair);
                    let pair_key = this.xmlDoc.createElement("Key");
                    pair_key.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'string')
                    pair_key.textContent = key
                    pair.appendChild(pair_key);
                    this.encodeNode(node[key], 'Value', pair, path, level + 2);
                }
            }

        }
        return elem;
    }

    encode_array(node, name, parent, path, level) {
        let elem

        if (!parent) { // корень
            elem = this.xmlDoc.createElement('Array');
            elem.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'Array')
        } else {
            elem = this.xmlDoc.createElement(name);
            elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'Array')
            parent.appendChild(elem);
        }
        for (let i = 0; i < node.length; i++) {
            this.encodeNode(node[i], 'Value', elem, path, level + 2);
        }
        return elem;
    }

    encode_ref(node, name, parent, path, level) {
        level -= 1;
        let elem = this.xmlDoc.createElement(name);
        let xsiLevel = `d${level}p1`;
        let xsiType = `${xsiLevel}:${node.ТипИС}.${node.ИмяИС}`;
        this.dp1NS[level] = true;
        // elem.setAttributeNS("http://www.w3.org/2000/xmlns/", `xmlns:${xsiLevel}`, "http://v8.1c.ru/8.1/data/enterprise/current-config");
        elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', xsiType);
        elem.textContent = node['ИдИС'];
        parent.appendChild(elem);
        return elem;

    }

    encode_str(node, name, parent) {
        // if (name === 'uid' && objHasOwnProperty(node, 'uid.xsi:type')) {
        //     return
        // }
        const elem = this.xmlDoc.createElement(name);
        elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'xs:string')
        elem.textContent = node;
        parent.appendChild(elem);
        return elem;
    }

    encode_num(node, name, parent) {
        const elem = this.xmlDoc.createElement(name);
        elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'xs:decimal')
        elem.textContent = node;
        parent.appendChild(elem);
        return elem;
    }

    encode_boolean(node, name, parent) {
        const elem = this.xmlDoc.createElement(name);
        elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'xs:boolean')
        elem.textContent = node ? 'true' : 'false';
        parent.appendChild(elem);
        return elem;
    }

    encode_null(node, name, parent) {
        const elem = this.xmlDoc.createElement(name);
        elem.setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:type', 'Null')
        parent.appendChild(elem);
        return elem;
    }

    loads(xmlStr) {
        // alert('window.DOMParser="' +typeof window.DOMParser + '"; window.ActiveXObject="' +typeof window.ActiveXObject +'"');
        if (typeof window.DOMParser !== 'undefined') {
            let result = new window.DOMParser().parseFromString(xmlStr, 'application/xml');
            const error = result.getElementsByTagName('parsererror');
            if (error.length) {
                throw new ExtException({
                    message: 'Error parseFromString',
                    detail: error[0].textContent,
                    dump: {xml: xmlStr}
                });
            }
            return result;
        } else if (typeof window.ActiveXObject !== 'undefined' && new window.ActiveXObject('Microsoft.XMLDOM')) {
            const xmlDoc = new window.ActiveXObject('Microsoft.XMLDOM');
            xmlDoc.async = 'false';
            xmlDoc.loadXML(xmlStr);
            return xmlDoc;
        } else {
            throw new Error('No XML parser found');
        }
    }


}
