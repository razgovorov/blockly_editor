import Connector from "../DomConnector";
import UrlParam from "bubot-helpers/UrlParam";
import ExtException from "bubot-helpers/ExtException";
import DemoData from "./DemoData";
import Xml from './XdtoSerializer';

export default class Connector1C extends Connector {
    initSerializer() {
        this.requestSerializer = new Xml()
        this.responseSerializer = this.requestSerializer
    }

    initConnector() {
        const self = this;
        const sendDomElem = document.createElement('div');
        window.domConnector = {
            sendDomElem,
            lastQueryId: 0,
            beginTime: Date.now(),
            timeout: Number(new UrlParam().get('timeout', 30)),
            attempt: 0,
            lock: false,
            resolve: this._onResolve,
            reject: this._onReject,
            request: {},
            log: function (action, data) {
                self.log(action, data);
            },
            delay: Number(new UrlParam().get('delay', 10))
        };
        sendDomElem.setAttribute('style', 'display: none');
        sendDomElem.setAttribute('id', 'toExtSys');
        document.querySelector('body').appendChild(sendDomElem);

        if (new UrlParam().get('test', null) === 'true') {
            this._demoData = new DemoData();
            sendDomElem.onclick = function (event) {
                event.stopPropagation();
                const connector = window.domConnector;
                let _action = event.target.getAttribute('action');
                // const data = new connector.request.Serializer().decode(event.target.innerHTML);
                let error = '<object><str name="message">Не найдены тестовые данные</str>';
                // error += '<str name="detail">' + _action + (data.hasOwnProperty('ClientId') ? '-' + data['ClientId'] : '') + '</str></object>';
                let xdtoSerializer = new Xml();
                if (!_action) {
                    connector._onReject(error);
                } else {
                    let result;
                    const handler = _action; //.replace('-', '_');
                    if (!self._demoData[handler]) {
                        self._onReject(error);
                        return;
                    }
                    try {
                        let data = self.requestSerializer.decode(event.target.textContext);
                        result = self._demoData[handler](data);
                        if (result === null) {
                            return;
                        }
                    } catch (err) {
                        result = null;
                        let err1 = new ExtException({parent: err, action: 'getDemoData'})
                        error = xdtoSerializer.encode(err1.toDict());
                    }
                    if (result) {
                        self._onResolve(result);
                    } else {
                        self._onReject(error);
                    }
                }
            };
        }
        return window.domConnector;
    }

    _send(connector, request) {
        try {
            request.action = `${request.objName}_${request.method}`.toUpperCase();
            connector.log(`send ${request.action}`, request.data);
            connector.sendDomElem.setAttribute('action', request.action);
            connector.sendDomElem.textContext = request.data
            connector.sendDomElem.click();
        } catch (err) {
            request.reject(new ExtException({
                message: "Send error",
                detail: err.message,
                parent: err,
                action: 'DomConnector._send',
                dump: {
                    call: request.action,
                    request: request.data,
                }
            }))
        }
    }

}