import axios from "axios";
import SapXmlSerializer from './SAP/SapXmlSerializer'


export default class Service {
    async init(data) {
        return await axios.get('/init', {params: data})
    }

    async commands(data) {
        let res = await axios.post('/commands', data)
        console.log(res.data)
        return res.data
    }

    async run(data, params) {
        let res
        let serializer = new SapXmlSerializer()
        data = upper_run_params(data, serializer)
        data = serializer.encode(data)
        const config = {
            headers: {
                'Content-Type': 'application/xml'
            },
            params
        }
        try {
            res = await axios.post('/sap/run', data, config)
        } catch (err) {
            return {'STATUS': 'error', 'COMMENT': err.response.data}
        }
        try {
            res = serializer.decode(res.data)
            res = lower_run_params(res, serializer)
            console.log(res)
            return res
        } catch (err) {
            return {'STATUS': 'error', 'COMMENT': err.message}
        }
    }

    async load(params) {
        return await axios.get('/load', {params})
    }

    async save(data, params) {
        return await axios.post('/save', data, {params})
    }

    async getListIni(params) {
        return await axios.get('/list_ini', {params})
    }

    log() {
    }
}


export function upper_run_params(data, serializer) {
    let lower = {}
    for (let key in data) {

        if (Object.prototype.hasOwnProperty.call(data, key)) {
            if (key === 'commands_result') {
                lower.COMMANDS_RESULT = []
                for (let i = 0; i < data[key].length; i++) {
                    let DATA = serializer.encode(data[key][i]['data'])
                    lower.COMMANDS_RESULT.push({
                        STATUS: data[key][i]['status'],
                        DATA,
                        UUID: data[key][i]['uuid']
                    })
                }
            } else {
                lower[key.toUpperCase()] = data[key]
            }

        }
    }
    return lower
}

export function lower_run_params(res, serializer) {
    let lower = {}
    for (let key in res) {

        if (Object.prototype.hasOwnProperty.call(res, key)) {
            if (key === 'COMMANDS') {
                lower.commands = []
                for (let i = 0; i < res['COMMANDS'].length; i++) {
                    let params = serializer.decode(res['COMMANDS'][i]['PARAMS'])
                    lower.commands.push({
                        name: res['COMMANDS'][i]['NAME'],
                        params,
                        uuid: res['COMMANDS'][i]['UUID']
                    })
                }
            } else {
                lower[key.toLowerCase()] = res[key]
            }

        }
    }
    return lower
}