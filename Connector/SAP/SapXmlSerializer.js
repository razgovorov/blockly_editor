import {objHasOwnProperty} from "bubot-helpers/BaseHelper"
import ExtException from "bubot-helpers/ExtException";

export default class SapXmlSerializer {

    constructor() {
        this.xmlNodes = ['array', 'object', 'str', 'num', 'bool', 'null'];
        this.jsonNodes = {
            array: 'array',
            object: 'object',
            null: 'null',
            number: 'num',
            boolean: 'bool',
            string: 'str'
        };
    }

    decode(xmlStr) {
        try {
            if (!xmlStr) {
                return;
            }
            const xml = this.loads(xmlStr);
            let result = this.decodeNode(xml.childNodes[0], null, '');
            return result;
        } catch (err) {
            throw new ExtException({
                parent: err,
                action: 'SapXmlSerializer.decode',
                // dump: {xml: xml_str}
            })

        }
    }

    decodeNode(node, parent, path) {
        if (this.xmlNodes.indexOf(node.nodeName) === -1) {
            throw new ExtException({
                message: 'decode error',
                detail: `Unsupported node type ${node.nodeName} ${path}`,
                action: 'SapXmlSerializer.decodeNode'
            });
        }
        const name = node.getAttribute('name');
        const handler = 'decode_' + node.nodeName
        if (!(handler in this)) {
            throw new ExtException({
                detail: `unsupported node type ${node.nodeName} ${path}`,
                action: 'SapXmlSerializer.decodeNode'
            });
        }

        const result = this[handler](node, parent, path + '.' + (name ? name : node.nodeName));
        if (name) {
            parent[name] = result;
            return;
        }
        return result;
    }

    decode_array(node, parent, path) {
        let result = [];
        const _nodes = node.childNodes;
        if (_nodes.length) {
            // alert('array'+ node.textContent);
            for (let i = 0; i < _nodes.length; i++) {
                try {
                    result.push(this.decodeNode(_nodes[i], null, `${path}.${i}`));
                } catch (err) {
                    throw new ExtException({parent: err});
                }
            }
        }
        return result;
    }

    decode_object(node, parent, path) {
        let result = {};
        const _nodes = node.childNodes;
        if (_nodes.length) {
            for (let i = 0; i < _nodes.length; i++) {
                try {
                    this.decodeNode(_nodes[i], result, path);
                } catch (err) {
                    throw new ExtException({parent: err});
                }
            }
        }
        return result;
    }

    decode_str(node) {
        return node.textContent;
    }

    decode_num(node) {
        return node.textContent;
    }

    decode_bool(node) {
        return !!node.textContent;
    }

    decode_null() {
        return null;
    }

    encode(data) {
        const root = this.encodeNode(data, null, null, '');
        return new XMLSerializer().serializeToString(root);
    }

    encodeNode(node, name, parent, path) {
        try {
            let node_type = typeof node;
            if (node_type === 'object') {
                if (node === null) {
                    node_type = 'null';
                } else {
                    if (!(node instanceof Object)) {
                        return
                    }
                    if (node instanceof Array) {
                        node_type = 'array';
                        // } else if (node instanceof Date) {
                        //     node_type = 'string';
                        //     node = node.toISOString();
                        // node = format(node, 'YYYY-MM-DDTHH:mm:ss.sss');
                    }
                }
            }
            if (!objHasOwnProperty(this.jsonNodes, node_type)) {
                return;
            }
            node_type = this.jsonNodes[node_type];
            const elem = document.createElement(node_type);
            if (parent) {
                parent.appendChild(elem);
            }
            if (name) {
                elem.setAttribute('name', name);
            }
            this['encode_' + node_type](node, name, elem, path + '.' + name ? name : node_type[0]);
            return elem;
        } catch (err) {
            throw new ExtException({parent: err, action: 'encodeNode'})
        }
    }

    encode_array(node, name, parent, path) {
        if (!node) {
            return;
        }
        for (let i = 0; i < node.length; i++) {
            this.encodeNode(node[i], null, parent, path);
        }
    }

    encode_object(node, name, parent, path) {
        if (!node) {
            return;
        }
        for (const key in node) {
            if (objHasOwnProperty(node, key)) {
                this.encodeNode(node[key], key, parent, path);
            }
        }
    }

    encode_str(node, name, parent) {
        parent.textContent = node;
    }

    encode_num(node, name, parent) {
        parent.textContent = node;
    }

    encode_bool(node, name, parent) {
        parent.textContent = node ? 1 : 0;
    }

    encode_null() {
    }

    loads(xmlStr) {
        // alert('window.DOMParser="' +typeof window.DOMParser + '"; window.ActiveXObject="' +typeof window.ActiveXObject +'"');
        if (typeof window.DOMParser !== 'undefined') {
            let result = new window.DOMParser().parseFromString(xmlStr, 'application/xml');
            const error = result.getElementsByTagName('parsererror');
            if (error.length) {
                throw new Error(error[0].textContent);
            }
            return result;
        } else if (typeof window.ActiveXObject !== 'undefined' && new window.ActiveXObject('Microsoft.XMLDOM')) {
            const xmlDoc = new window.ActiveXObject('Microsoft.XMLDOM');
            xmlDoc.async = 'false';
            xmlDoc.loadXML(xmlStr);
            return xmlDoc;
        } else {
            throw new Error('No XML parser found');
        }
    }

}
