import Xml from './SapXmlSerializer';

const test_data_calc_ini = {
    '_material_read_2511КТ197': {
        "HEADER": {
            "MBLNR": "5000000293",
            "MJAHR": "2021",
            "NUMBER": "2511КТ197",
            "DATE": "20201125"
        },
        "ITEMS": [{
            "BUKRS": "ZUCH",
            "LIFNR": "0000000001",
            "MATNR": "000000000000100067",
            "MATNR_LIFNR": "5300000071",
            "COUNT": "14.000 ",
            "PRICE": "30.00 "
        }]
    },
    '_material_read_2222': {
        "HEADER": [{"MBLNR": "5000000289", "MJAHR": "2021", "NUMBER": "11111", "DATE": "20201125"}],
        "ITEMS": [{
            "BUKRS": "ZUCH",
            "LIFNR": "0000000001",
            "MATNR": "000000000000100000",
            "MATNR_LIFNR": "5300000000",
            "COUNT": "1000.000 ",
            "PRICE": "120.00 "
        }]
    },
    '_material_read_11111': {
        "HEADER": [{"MBLNR": "5000000289", "MJAHR": "2021", "NUMBER": "11111", "DATE": "20201125"}],
        "ITEMS": [{
            "BUKRS": "ZUCH",
            "LIFNR": "0000000001",
            "MATNR": "000000000000100000",
            "MATNR_LIFNR": "5300000000",
            "COUNT": "1000.000 ",
            "PRICE": "120.00 "
        }]
    },
    '_Контрагент_find': '1',
    '_Организация_find': '2'
}

export default class DemoData {
    BLOCKLY_RUN(data) {
        // const f1 = '<object xmlns="http://www.w3.org/1999/xhtml"><str name="CONNECTION_UUID">45e37dca-1a09-11ec-9621-0242ac130002</str><str name="INI_NAME">Blockly_Blockly_Robot</str><str name="ENDPOINT"></str><str name="OPERATION_UUID"></str><array name="COMMANDS_RESULT"></array><bool name="DEBUG">1</bool><str name="STEP"></str><null name="SELECTED"></null></object>'
        // const f2 = '<object><str name="BEGIN">2021-12-07 16:22:20</str><array name="COMMANDS"><object><str name="NAME">update_ini</str><str name="PARAMS">&lt;object&gt;&lt;str name="name"&gt;check_ebeln&lt;/str&gt;&lt;num name="version"&gt;0&lt;/num&gt;&lt;str name="value"&gt;&amp;lt;xml xmlns="https://developers.google.com/blockly/xml"&amp;gt;&amp;lt;variables&amp;gt;&amp;lt;variable id="##,SWwdp|@Z{mOc0iOO{"&amp;gt;PO&amp;lt;/variable&amp;gt;&amp;lt;variable id="#{cqgw~:?=0~7iucFP^w"&amp;gt;GR&amp;lt;/variable&amp;gt;&amp;lt;variable id="z[Q0I^`Z2mjN;@-NA4{I"&amp;gt;result&amp;lt;/variable&amp;gt;&amp;lt;/variables&amp;gt;&amp;lt;block type="procedures_defreturn" id="1}EX$^?Q$#`FMobcccW!" x="-187" y="163"&amp;gt;&amp;lt;mutation&amp;gt;&amp;lt;arg name="PO" varid="##,SWwdp|@Z{mOc0iOO{"&amp;gt;&amp;lt;/arg&amp;gt;&amp;lt;arg name="GR" varid="#{cqgw~:?=0~7iucFP^w"&amp;gt;&amp;lt;/arg&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="NAME"&amp;gt;main&amp;lt;/field&amp;gt;&amp;lt;comment pinned="false" h="80" w="160"&amp;gt;Describe this function...&amp;lt;/comment&amp;gt;&amp;lt;statement name="STACK"&amp;gt;&amp;lt;block type="variables_set" id="vfsl0y7qBjo~JP)_h8fb"&amp;gt;&amp;lt;field name="VAR" id="z[Q0I^`Z2mjN;@-NA4{I"&amp;gt;result&amp;lt;/field&amp;gt;&amp;lt;value name="VALUE"&amp;gt;&amp;lt;block type="sap_call_select" id="z.Rz4ahB2/L?:UQL2%=x"&amp;gt;&amp;lt;mutation items="1"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="FIELD0_NAME"&amp;gt;MKPF~MBLNR&amp;lt;/field&amp;gt;&amp;lt;field name="FIELD0_AS"&amp;gt;MBLNR&amp;lt;/field&amp;gt;&amp;lt;value name="from"&amp;gt;&amp;lt;block type="concatenate" id="4bqJkVMbqn9d:#%$t7Bq"&amp;gt;&amp;lt;mutation items="3"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="SEPARATOR"&amp;gt;space&amp;lt;/field&amp;gt;&amp;lt;value name="PROP0"&amp;gt;&amp;lt;block type="text" id="Mr-(:M6Tc`R|1L;onzo_"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MKPF&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP1"&amp;gt;&amp;lt;block type="text" id="=2{GuAi2}5bdlh_O*I.6"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;INNER JOIN&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP2"&amp;gt;&amp;lt;block type="concatenate" id="0`G#1!Hvt8CzIlB]lm7f"&amp;gt;&amp;lt;mutation items="3"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="SEPARATOR"&amp;gt;space&amp;lt;/field&amp;gt;&amp;lt;value name="PROP0"&amp;gt;&amp;lt;block type="text" id="]fqn4X@bZ2Bu|Vl[`2QR"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MSEG&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP1"&amp;gt;&amp;lt;block type="text" id="73QHs/1~N8LAKyojAe{J"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;ON&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP2"&amp;gt;&amp;lt;block type="text" id="ll^cDRHp-4}_O`VE%Zg."&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MKPF~MBLNR = MSEG~MBLNR&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="where"&amp;gt;&amp;lt;block type="concatenate" id="wZH?@XrTqwmg3G$AhAh,"&amp;gt;&amp;lt;mutation items="3"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="SEPARATOR"&amp;gt;space&amp;lt;/field&amp;gt;&amp;lt;value name="PROP0"&amp;gt;&amp;lt;block type="concatenate" id="q_f;klC-~`uUD-hh+~q:"&amp;gt;&amp;lt;mutation items="3"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="SEPARATOR"&amp;gt;&amp;lt;/field&amp;gt;&amp;lt;value name="PROP0"&amp;gt;&amp;lt;block type="text" id="X;BP#^.xJ:fT1n==SIlX"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MSEG~EBELN = \'&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP1"&amp;gt;&amp;lt;block type="variables_get" id="?{j=M/kP2iRvxD3$Y?#x"&amp;gt;&amp;lt;field name="VAR" id="##,SWwdp|@Z{mOc0iOO{"&amp;gt;PO&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP2"&amp;gt;&amp;lt;block type="text" id="^.E3gNt7D!z_E=!XU*Yk"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;\'&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP1"&amp;gt;&amp;lt;block type="text" id="e,MRq6Q1TET4Ov:7*=$t"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;AND&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP2"&amp;gt;&amp;lt;block type="concatenate" id="t_uwRfN|?hKUz3=U+U^y"&amp;gt;&amp;lt;mutation items="3"&amp;gt;&amp;lt;/mutation&amp;gt;&amp;lt;field name="SEPARATOR"&amp;gt;&amp;lt;/field&amp;gt;&amp;lt;value name="PROP0"&amp;gt;&amp;lt;block type="text" id="jGH?gx{@=BjZXdmitd%x"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MKPF~FRBNR = \'&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP1"&amp;gt;&amp;lt;block type="variables_get" id="[glg|#B1IY:c[Hj0c[u+"&amp;gt;&amp;lt;field name="VAR" id="#{cqgw~:?=0~7iucFP^w"&amp;gt;GR&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PROP2"&amp;gt;&amp;lt;block type="text" id="^m1642|4G$3ck{ok/,l!"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;\'&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="limit"&amp;gt;&amp;lt;block type="text" id="VwGntu#TMfIe|y,{k{^*"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;1&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="offset"&amp;gt;&amp;lt;block type="text" id="HluWs0Hk}JHe0F~C{%?m"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;1&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;value name="PARAM0"&amp;gt;&amp;lt;block type="text" id="})B(aF[=dPh2}p%j_K5p"&amp;gt;&amp;lt;field name="TEXT"&amp;gt;MBLNR&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/statement&amp;gt;&amp;lt;value name="RETURN"&amp;gt;&amp;lt;block type="variables_get" id="F{!]?!%P3=]IU.99:bd~"&amp;gt;&amp;lt;field name="VAR" id="z[Q0I^`Z2mjN;@-NA4{I"&amp;gt;result&amp;lt;/field&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/value&amp;gt;&amp;lt;/block&amp;gt;&amp;lt;/xml&amp;gt;&lt;/str&gt;&lt;/object&gt;</str><str name="UUID" /></object><object><str name="NAME">calc_ini</str><str name="PARAMS">&lt;object&gt;&lt;str name="ini_name"&gt;check_ebeln&lt;/str&gt;&lt;str name="PO"&gt;4500000257&lt;/str&gt;&lt;str name="GR"&gt;TR NAKL&lt;/str&gt;&lt;/object&gt;</str><str name="UUID">40dd3349-3ea2-4365-bf52-3d65afb6aba8</str></object><object><str name="NAME">calc_ini</str><str name="PARAMS">&lt;object&gt;&lt;str name="ini_name"&gt;check_ebeln&lt;/str&gt;&lt;str name="PO"&gt;4500000257&lt;/str&gt;&lt;str name="GR"&gt;TR NAKL&lt;/str&gt;&lt;/object&gt;</str><str name="UUID">a83bfdba-59f7-42e6-b5b7-e872ed3aff7a</str></object><object><str name="NAME">calc_ini</str><str name="PARAMS">&lt;object&gt;&lt;str name="ini_name"&gt;check_ebeln&lt;/str&gt;&lt;str name="PO"&gt;4500000257&lt;/str&gt;&lt;str name="GR"&gt;TR NAKL&lt;/str&gt;&lt;/object&gt;</str><str name="UUID">5b725830-e633-498b-ac31-37e10aa53735</str></object><object><str name="NAME">calc_ini</str><str name="PARAMS">&lt;object&gt;&lt;str name="ini_name"&gt;check_ebeln&lt;/str&gt;&lt;str name="PO"&gt;4501242712&lt;/str&gt;&lt;str name="GR"&gt;351&lt;/str&gt;&lt;/object&gt;</str><str name="UUID">ec04ed12-8318-4e2a-addf-aab613b3dc37</str></object></array><str name="CONNECTION_UUID">45e37dca-1a09-11ec-9621-0242ac130002</str><str name="OPERATION_UUID">9cb58f57-b1e1-4071-a52f-de2733f04442</str><str name="STATUS">run</str><bool name="STEPBYSTEP">0</bool></object>'
        console.log(data)
        const f3 = {
            "COMMANDS": [],
            "STATUS": "error",
            "OPERATION_UUID": "58389cf8-f5c8-411b-8a85-c87d38c797bb",
            "CONNECTION_UUID": "45e37dca-1a09-11ec-9621-0242ac130002",
            "STEPBYSTEP": true,
            "BEGIN": "2021-12-09 15:00:38",
            "DATA": {
                "__name__": "ExtException",
                "__module__": "blockly_executor.ext_exception",
                "message": "Unknown error",
                "detail": "<str>&lt;str xmlns=\"http://www.w3.org/1999/xhtml\"&gt;Ошибка обработки ини&lt;/str&gt;</str>",
                "code": "3000",
                "action": "",
                "dump": {
                    "block_context": {
                        "__path": ".",
                        "INI_NAME": "check_ebeln",
                        "PARAM0_VALUE": "4500000257",
                        "PARAM1_VALUE": "TR NAKL",
                        "PARAM0_NAME": "PO",
                        "PARAM1_NAME": "GR",
                        "__deferred": "a7b9afa1-0bac-48ca-88e5-a3c4a647b774"
                    },
                    "block_id": "^-3EcBZW4*bpX:UCur/?",
                    "full_name": "ExtsysCalcIni"
                },
                "stack": [
                    {
                        "message": "Exception",
                        "detail": "<str>&lt;str xmlns=\"http://www.w3.org/1999/xhtml\"&gt;Ошибка обработки ини&lt;/str&gt;</str>",
                        "traceback": "extsys_calc_ini.py, _calc_value, line 20",
                        "action": ""
                    }
                ]
            }
        }
        return new Xml().encode(f3)
        // if (data)
        //     return f1
        // return f2
    }
    BLOCKLY_COMMANDS(data) {
        let result = []
        let xml = new Xml()
        for (let i = 0; i < data.length; i++) {
            if (typeof this[data[i].name] === 'function') {
                try {
                    const params = xml.decode(data[i].params)
                    let _res = this[data[i].name](params)
                    if (data[i].uuid) {
                        result.push({DATA: xml.encode(_res), UUID: data[i].uuid, STATUS: 'complete'})
                    }
                } catch (err) {
                    if (data[i].uuid) {
                        result.push({DATA: xml.encode(err.message), UUID: data[i].uuid, STATUS: 'error'})
                    }
                }
            } else {
                // f
            }

        }
        return new Xml().encode(result)
    }

    update_ini() {
    }

    calc_ini(params) {
        let uid = ''
        for (let param in params)
            if (Object.prototype.hasOwnProperty.call(params, param)) {
                if (params[param] instanceof Object) {
                    continue
                }
                uid += '_' + params[param]

            }
        let result = test_data_calc_ini[uid]
        if (result) {
            return result
        }
        throw new Error(`test data not defined calc_ini(${uid})`)
    }
}
