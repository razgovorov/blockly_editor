import {objHasOwnProperty} from "bubot-helpers/BaseHelper"
import Connector from "../DomConnector";
import UrlParam from "bubot-helpers/UrlParam";
import {lower_run_params, upper_run_params} from "../LocalXml"
// import ExtException from "bubot-helpers/ExtException";
import DemoData from "./DemoData";
import Xml from './SapXmlSerializer';
// import XmlExt from './SapXmlExtendedSerializer';


export default class ConnectorSap extends Connector {

    initSerializer() {
        this.requestSerializer = new Xml()
        this.responseSerializer = new Xml()
    }


    async initConnector() {
        const self = this;
        const sendDomElem = document.createElement('form');
        window.domConnector = {
            sendDomElem,
            lastQueryId: 0,
            beginTime: Date.now(),
            timeout: Number(new UrlParam().get('timeout', 30)),
            attempt: 0,
            lock: false,
            resolve: this._onResolve,
            reject: this._onReject,
            request: {},
            log: function (action, data) {
                self.log(action, data);
            },
            delay: Number(new UrlParam().get('delay', 10))
        };
        window.domConnectorSap = window.domConnector; //временно пока CocaCola не обновим
        sendDomElem.setAttribute('style', 'display: none');
        sendDomElem.setAttribute('name', 'ExchangeForm');
        sendDomElem.setAttribute('method', 'post');
        sendDomElem.setAttribute('action', 'SAPEVENT:SABYCALL');
        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('name', 'data');
        window.domConnector.data = input;
        sendDomElem.appendChild(input);
        const input2 = document.createElement('input');
        input2.setAttribute('type', 'submit');
        sendDomElem.appendChild(input2);
        document.querySelector('body').appendChild(sendDomElem);

        if (new UrlParam().get('test', null) === 'true') {
            this._demoData = new DemoData();
            sendDomElem.submit = function () {
                const connector = window.domConnector;
                const data = self.requestSerializer.decode(this.data.value);
                let _action = this.getAttribute('action').substring(9);
                let error = '<object><str name="message">Не найдены тестовые данные</str>';
//                error += '<str name="detail">' + _action + (objHasOwnProperty(data, 'ClientId') ? '-' + data['ClientId'] : '') + '</str></object>';
                if (!_action) {
                    connector._onReject(error);
                } else {
                    let result;
                    const handler = _action.replace('-', '_');
                    if (!self._demoData[handler]) {
                        self._onReject(error);
                        return;
                    }
                    try {
                        result = self._demoData[handler](data);
                        if (result === null) {
                            return;
                        }
                    } catch (err) {
                        result = null;
                        error = `<object><str name="message">${err}</str><str name="detail">${_action}  ${(objHasOwnProperty(data, 'ClientId') ? '-' + data['ClientId'] : '')}</str></object>`;
                    }
                    if (result) {
                        self._onResolve(result);
                    } else {
                        self._onReject(error);
                    }
                }
            };
        }
        return window.domConnector;
    }

    _send(connector, request) {
        request.action = `SAPEVENT:${request.objName}-${request.method}`.toUpperCase();
        connector.log(`send ${request.action}`, request.data);
        connector.sendDomElem.setAttribute('action', request.action);
        connector.data.value = request.data;
        connector.sendDomElem.submit();
    }

    async commands(data) {
        let i
        for (i = 0; i < data.length; i++) {
            if (!objHasOwnProperty(data[i], 'uuid')) {
                data[i]['uuid'] = ''
            }
            let _params = this.requestSerializer.encode(data[i]['params'])
            data[i]['params'] = _params.replace('xmlns="http://www.w3.org/1999/xhtml"', '')
        }
        let res = await this.call('blockly', 'commands', data, new Xml(), new Xml(), 0)
        // console.log('res1')
        // console.log(res)
        if (! (res instanceof Array)){
            throw new Error('command response is not array')
        }

        let result = []
        for (i = 0; i < res.length; i++) {
            let status = res[i]['STATUS']
            let data = res[i]['DATA']
            // if (status === 'complete') {
            //     try {
            //         data = this.requestSerializer.decode(res[i]['DATA'])
            //     } catch (e) {
            //         status = 'error'
            //         data = `decode command error: ${e}`
            //     }
            // }
            result.push({
                uuid: res[i]['UUID'],
                status,
                data
            })
        }
        // console.log('res2')
        // console.log(result)
        return result
    }

    async run(data) {
        data = upper_run_params(data, this.requestSerializer)
        let res = await this.call('blockly', 'run', data, this.requestSerializer, this.responseSerializer, 0)
        res = lower_run_params(res, this.requestSerializer)
        return res
    }
}

