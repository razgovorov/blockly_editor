import SapXmlSerializer from './SapXmlSerializer';
import {objHasOwnProperty} from "bubot-helpers/BaseHelper"

export default class SapXmlExtendedSerializer extends SapXmlSerializer {

    decodeNode(node, parent, path) {
        if (this.xmlNodes.indexOf(node.nodeName) === -1) {
            throw new Error(`Unsupported node type ${node.nodeName} ${path}`);
        }
        // throw new Error("unsupported node: " + node.nodeName + ", path:" + path + ", content:" + node.textContent + ",");
        const name = node.getAttribute('name');
        const _type = node.getAttribute('type');
        const result = this['decode_' + node.nodeName](node, parent, path + '.' + (name ? name : node.nodeName));
        if (_type) {
            parent[name] = {type: _type};
            if (name) {
                parent[name]['_value'] = result;
            }
        } else if (name) {
            parent[name] = result;
        }
        return result;
    }

    encodeNode(node, name, parent, path) {
        let _node = node
        if (name && node && node['_value'] !== undefined) {
            _node = node['_value'];
        }

        let node_type = typeof _node;
        if (node_type === 'object') {
            if (_node === null) {
                node_type = 'null';
            } else {
                if (!(_node instanceof Object)) {
                    return
                }
                if (_node instanceof Array) {
                    node_type = 'array';
                    // } else if (node instanceof Date) {
                    //     node_type = 'string';
                    //     // node = node.toISOString();
                    //     _node = format(_node, 'YYYY-MM-DDTHH:mm:ss.sss');
                }
            }
        }
        if (!objHasOwnProperty(this.jsonNodes, node_type)) {
            return;
        }
        node_type = this.jsonNodes[node_type];
        const elem = document.createElement(node_type);
        if (parent) {
            parent.appendChild(elem);
        }
        if (name) {
            elem.setAttribute('name', name);
        }
        if (node && objHasOwnProperty(node, 'type')) {
            elem.setAttribute('type', node['type']);
        }
        this['encode_' + node_type](_node, name, elem, path + '.' + name ? name : node_type[0]);
        return elem;

    }

}
