import ExtException from "bubot-helpers/ExtException";
import axios from "axios";
import {post} from "@/BlocklyEditor/Connector/CurrentHost";

export default class DomConnector {
    param = {}
    _log = []

    constructor(log, appName) {
        this._log = log
        this.appName = appName
        this.path = 'Algorithm/'
        this.initSerializer()
        this.log('useragent', window.navigator.userAgent)
    }
    initSerializer() {
        this.requestSerializer = undefined
        this.responseSerializer = undefined
    }

    async init(data) {
        return await post(`/${this.appName}/api/${this.path}blockly_editor_init`, data)
    }

    async call(objName, method, data, RequestSerializer, ResponseSerializer, timeout = undefined) {
        try {
            let connector = window.domConnector;
            if (!connector) {
                connector = await this.initConnector();
            }
            timeout = data && data.timeout ? data.timeout : timeout;
            timeout = timeout === undefined ? connector.timeout : timeout;
            connector.log(`call ${method} ${objName}`, `timeout: ${timeout}`);
            if (connector.lock) {
                alert('Запрос до окончания предыдущего');
                return;
            }
            connector.lastQueryId++;
            connector.request = {
                serializer: this.responseSerializer,
                objName: objName,
                method: method,
                attempt: 0,
                data: '',
                queryId: connector.lastQueryId,
            }
            const callPromise = new Promise((resolve, reject) => {
                connector.request.resolve = resolve
                connector.request.reject = reject
            });
            try {
                connector.request.data = this.requestSerializer.encode(data);
            } catch (err) {
                connector.log(`Error encode`, err.toString());
                connector.request.reject(new ExtException({
                    message: "call error encode",
                    parent: err,
                    action: 'DomConnector.call',
                    dump: {
                        objName: objName,
                        method: method,
                        data: data,
                    }
                }))
                return callPromise
            }
            this._call(method, timeout);
            return callPromise;
        } catch (err) {
            throw new ExtException({action: `${this._className}.call`, parent: err});
        }
    }

    _call(method, timeout) {
        try {
            let connector = window.domConnector;
            let request = window.domConnector.request;
            const self = this;
            const __call = function (queryId, timeout) {
                connector.beginTime = Date.now();
                connector.lock = true;
                self._send(connector, request)
                if (timeout) {
                    setTimeout(function (queryId, timeout) {
                            if (connector.lastQueryId !== queryId || !connector.lock) {  // запрос прошел все хорошо ничего не надо
                                return;
                            }
                            connector.log(`check timeout ${request.action}`);
                            request.attempt += 1;
                            if (request.attempt > 0) {
                                connector.log(`timeout ${request.action}`);
                                request.reject(new ExtException({
                                    message: `timeout ${request.action}`,
                                    dump: {data: request.data}
                                }))
                                connector.request = {};
                                connector.lock = false
                                return;
                            }
                            self._call(method, timeout);

                        }, timeout * 1000, queryId, timeout
                    )
                }
            };
            if (connector.delay) {
                // иначе SAP не успевает, подозреваю что цифра здесь может быть любая. Главное управление передать. пока не понятно
                setTimeout(__call, Math.max(connector.delay - Date.now() - connector.beginTime, 10), connector.lastQueryId, timeout);
            } else {
                __call(connector.lastQueryId, timeout);
            }
        } catch (err) {
            throw new ExtException({action: `${this._className}._call`, parent: err});
        }
    }

    _onResolve(result) {
        let connector = window.domConnector;
        let request = window.domConnector.request;
        if (!request) {
            alert('Поступил ответ без запроса !')
            return;
        }
        try {
            const data = result ? request.serializer.decode(result) : null;
            connector.log(`resolve ${request.action}`, JSON.stringify(data, null, ' '));
            request.resolve(data);
        } catch (err) {
            connector.log(`resolve ${request.action}`, result);
            connector.log(`Decode error`, err.toString());
            request.reject(new ExtException({
                message: "XML decode error",
                detail: `${err.message}`,
                parent: err,
                action: 'SapDomConnector._onResolve',
                dump: {
                    call: request.action,
                    request: request.data,
                    response: result
                }
            }))
        }
        window.domConnector.request = {};
        window.domConnector.lock = false
    }

    _onReject(result) {
        let connector = window.domConnector;
        let request = window.domConnector.request;
        if (!request) {
            alert('Поступил ответ без запроса !')
            return;
        }
        try {
            connector.log(`reject ${request.action}`, result);
            const data = result ? request.serializer.decode(result) : {message: 'empty reject'};
            const _error = new ExtException(data);
            request.reject(_error);
        } catch (err) {
            request.reject(new ExtException({
                parent: err,
                action: 'SapDomConnector._onReject',
                dump: {
                    call: request.action,
                    request: request.data,
                    response: result
                }
            }))
        }
        window.domConnector.request = {};
        window.domConnector.lock = false
    }

    log(action, data) {
        // if (!this.debug)
        //     return;
        this._log.unshift({
            // id: this._logLastId++,
            // time: format(new Date(), 'HH:mm:ss.SSS'),
            action: action,
            data: data || ''
        })
        if (this._log.length > 30)
            this._log.pop();
    }

    async commands(data) {
        try {
            return await this.call('blockly', 'commands', data, this.requestSerializer, this.responseSerializer, 0)
        } catch (err) {
            throw new ExtException({parent:err})
        }
    }

    async run(data) {
        let res = await this.call('blockly', 'run', data, this.requestSerializer, this.responseSerializer, 0)
        return res
    }

    async updateIni(data) {
        let res = await this.call('blockly', 'update_ini', data, this.requestSerializer, this.responseSerializer, 0)
        return res
    }

    async load(params) {
        return await axios.get('/load', {params})
    }

    async save(data, params) {
        return await axios.post('/save', data, {params})
    }
}