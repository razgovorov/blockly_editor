import Blockly from "blockly";
import {updateObject} from "bubot-helpers/BaseHelper"
import ExtException from "bubot-helpers/ExtException";

export default {
    data() {
        return {
            gluerResult: {}
        }
    }, methods: {
        onSelectIni(event) {
            this.iniListVisible = false
            this.load(event.name, event.own, event.file_type)
        },
        load: async function (workspaceName, workspaceOwn, workspaceType) {
            let workspaceData = await this.request_workspace(workspaceName, workspaceOwn, workspaceType)
            let workspace = this.getWorkSpace();

            // workspace.setTheme(this.theme)

            workspace.clear();
            if (!workspaceData) {
                console.log('return empty workspace')
                return
            }
            workspaceType = typeof workspaceData === "string" ? "XML" : "JSON"
            this[`loadWorkspace${workspaceType}`](workspace, workspaceData)
            workspace.scrollCenter()
            this.showBreakpoints()
        },
        request_workspace: async function (workspace_name, workspace_own, workspace_type) {
            let param
            if (workspace_name) {
                param = {Algorithm: workspace_name}
                param.AlgorithmOwn = workspace_own
                if (workspace_type) {
                    param.AlgorithmFileType = workspace_type
                }
                param = updateObject({}, this.settings, param)
            } else {
                param = this.settings
            }
            if (!param.Algorithm)
                return
            try {
                let resp = await this.service.load(param)
                // console.log(`${this.currentAlgorithm} - ${param.algorithm}`)
                this.gluerResult = resp.data['gluer_result']
                this.settings = Object.assign({}, this.settings, {
                    Algorithm: resp.data['name'],
                    AlgorithmFileType: resp.data['file_type'],
                    AlgorithmOwn: resp.data['own'],
                    AlgorithmBase: resp.data['base']
                })
                this.currentAlgorithm = this.settings.Algorithm
                this.updateUrlParam()
                return resp.data.workspace
            } catch (err) {
                this.status = 'error'
                this.result = err
                return '<xml xmlns="https://developers.google.com/blockly/xml"></xml>'
            }
        },
        loadWorkspaceJSON: function (workspace, workspaceData) {
            if (typeof workspaceData !== 'object') {
                throw new Error('Workspace not json')
            }
            Blockly.serialization.workspaces.load(workspaceData, workspace);
        },
        loadWorkspaceXML: function (workspace, workspaceData) {
            if (typeof workspaceData === 'string') {
                // костыли для правки косяков
                if (workspaceData[0] === '{') {
                    // видимо по ошибке влили json читаем
                    this.loadJsonWorkspace(workspace, JSON.parse(workspaceData))
                    return
                }
            } else {
                this.loadJsonWorkspace(workspace, workspaceData)
            }

            let xml = Blockly.utils.xml.textToDom(workspaceData);
            Blockly.Xml.domToWorkspace(xml, workspace);
        },
        download: function () {
        },
        saveAs: async function (variant) {
            this.settings.Algorithm = this.currentAlgorithm
            this.settings.AlgorithmOwn = variant
            this.updateUrlParam()
            await this.save()

        },
        save: async function () {
            let workspaceData = this[`getWorkspaceData${this.settings.AlgorithmFileType.toUpperCase()}`]()
            if (!this.currentAlgorithm) {
                this.status = 'error'
                this.result = new ExtException({message: 'Не указано имя алгоритма'})
                return
            }
            await this.service.save({
                name: this.currentAlgorithm,
                base: this.settings.AlgorithmBase,
                own: this.settings.AlgorithmOwn,
                type: this.settings.AlgorithmFileType,
                workspace: workspaceData
            }, this.settings)

            // сохранение копии в json
            // if (this.settings.AlgorithmFileType !== 'json') {
            //     let workspaceData = this[`getWorkspaceDataJson`]()
            //     await this.service.save({
            //         name: this.currentAlgorithm,
            //         base: this.settings.AlgorithmBase,
            //         own: this.settings.AlgorithmOwn,
            //         type: 'json',
            //         workspace: workspaceData
            //     }, this.settings)
            // }
        },
        getWorkspaceDataJSON: function () {
            return Blockly.serialization.workspaces.save(this.getWorkSpace());
        },
        getWorkspaceDataXML: function () {
            let xml = Blockly.Xml.workspaceToDom(this.getWorkSpace());
            // sap 1c не поддерживают xml с отступами
            // return Blockly.Xml.domToPrettyText(xml);
            return new XMLSerializer().serializeToString(xml)
        },
        updateCurrentIni: async function () {
            let workspaceData = this[`getWorkspaceData${this.settings.AlgorithmFileType.toUpperCase()}`]()
            await this.updateIni(this.settings.Algorithm, workspaceData)
        },
        updateChanges: async function () {
            console.log('updateChanges')
            let workspaceData = this[`getWorkspaceData${this.settings.AlgorithmFileType.toUpperCase()}`]()
            let resp = await this.service.compare({
                name: this.settings.Algorithm,
                own: this.settings.AlgorithmOwn,
                file_type: this.settings.AlgorithmFileType,
                base: this.settings.AlgorithmBase,
                data: workspaceData
            }, this.settings)
            this.gluerResult = resp.data
        },
        onCreateIni(event) {
            this.iniListVisible = false
            console.log(event)
            this.settings.Algorithm = event
            this.currentAlgorithm = this.settings.Algorithm
            this.updateUrlParam()
            let workspace = this.getWorkSpace();
            workspace.clear();
        },

    }
}

export function hideChanges(blocklyRef) {
    Array.from(blocklyRef.$el.getElementsByClassName('gluerChange')).forEach(
        (el) => el.classList.remove('gluerChange', 'gluerCurrentChange')
    );
}