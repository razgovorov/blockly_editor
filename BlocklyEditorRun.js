import ExtException from "bubot-helpers/ExtException";
import {objHasOwnProperty, isEmptyObject} from "bubot-helpers/BaseHelper"


export default {
    data() {
        return {
            selectedBlock: '', // идентификатор выделенного блока
            running: false, // Алгорим исполняется во внешней системе
            context: {
                current_workspace: null, // имя алгоритма на котором закончился предыдущий вызов. Во
                //время выполнения могут вызываться вложенные алгоритмы
                current_block: null, // идентификатор блока на котором закончился предыдущий вызов,
                //при первом вызове не указывается или пустая строка (только для отладки).
                breakpoints: {}, // точки основа {алгоритм{идентификатор блока{параметры точки}}
                commands_result: null, //массив объектов с результатами выполнения внешних команд
                debug_mode: null, // - режим отладки, step - пошаговый, resume - идем до следующей точки останова
                uid: null,
                raw: null
            },
            result: null,
            current_variables: null
        }
    },
    computed: {
        breakpoints() {
            if (!Object.prototype.hasOwnProperty.call(this.context.breakpoints, this.settings.Algorithm)) {
                return undefined
            }
            return this.context.breakpoints[this.settings.Algorithm]
        },
        breakpointAction() {
            if (this.selectedBlock) {
                if (!this.breakpoints) {
                    return false
                }
                return this.breakpoints.indexOf(this.selectedBlock) >= 0
            }
            return undefined
        }
    },
    methods: {
        stopDebugging: async function () {
            this.context.debug_mode = null

            let currentBlockNode = this.$el.querySelector('.currentBlock')
            if (currentBlockNode) {
                currentBlockNode.classList.remove('currentBlock')
            }

            this.currentBlock = ''
            if (this.settings.Algorithm !== this.context.algorithm) {
                this.settings.Algorithm = this.context.algorithm
                await this.load()
            }
            let workspace = this.getWorkSpace()
            workspace.highlightBlock('');
            this.debugProcessed = false
            this.debugPanelVisible = false
        },

        beforeRunning: async function (debugMode) {
            this.context.current_block = null
            this.context.algorithm = this.settings.Algorithm
            if (!this.settings.Algorithm) {
                throw new ExtException({message: 'Не указано имя алгоритма'})
            }
            this.context.current_workspace = this.settings.Algorithm
            this.context.commands_result = null
            this.context.uid = null
            this.context.raw = null
            if (debugMode) {
                this.context.debug_mode = debugMode
                this.debugPanelVisible = true
                this.debugPanelTab = 1
                if (debugMode === 'debug') {
                    if (isEmptyObject(this.breakpoints)) {
                        // если нет точек останова запускаемся в пошаговом режиме
                        this.context.debug_mode = 'step'
                    } else {
                        // если есть точки останова идем до первой
                        this.context.debug_mode = 'resume'
                    }
                }
            }
            await this.save();

        },
        run: async function (debugMode) {
            if (!debugMode || (debugMode && !this.context.debug_mode)) {  // первый запуск
                try {
                    await this.beforeRunning(debugMode)
                } catch (err) {
                    this.status = 'error'
                    this.result = new ExtException({parent: err})
                    await this.stopDebugging()
                    return
                }

            } else {
                this.context.debug_mode = debugMode
            }

            try {
                let workspace = this.getWorkSpace()
                for (; ;) {
                    let resp
                    this.running = true
                    try {
                        resp = await this.connector.run(Object.assign(
                            {},
                            this.urlParam.param,
                            {
                                CurrentBlock: this.context.current_block,
                                CurrentWorkspace: this.context.current_workspace,
                                DebugMode: this.context.debug_mode,
                                ContextId: this.context.uid,
                                Breakpoints: this.context.breakpoints,
                                CommandsResult: this.context.commands_result,
                                // Endpoint:this.context.
                                // EndpointArgs:this.context.
                                // Algorithm:this.context.
                                // AlgorithmOwn:this.context.
                                // AlgorithmFileType:this.context.
                            }
                        ))
                    } catch (err) {
                        this.status = 'error'
                        this.result = new ExtException({parent: err})
                        await this.stopDebugging()
                        return
                    }
                    this.status = resp['Status']
                    if (!resp || !this.status) {  // если вернулся невалидный ответ
                        this.status = 'error'
                        this.result = {
                            message: 'Невалидный ответ от коннектора',
                            detail: resp
                        }
                        await this.stopDebugging()
                        return
                    }
                    if (await this._checkUpdateIni(resp)) {
                        continue
                    }
                    this.context.uid = resp['ContextId']
                    this.context.raw = resp['raw']
                    this.result = resp['Result']
                    this.current_variables = resp['CurrentVariables']
                    await this._executeCommands(resp['Commands'])
                    this.running = false;
                    let currentBlock = resp['CurrentBlock'] || ''
                    this.context.current_workspace = resp['CurrentWorkspace']
                    await this.showCurrentBlock(workspace, currentBlock)
                    if (resp['Status'] !== 'run') {
                        this.result = resp['Result']
                        await this.stopDebugging()

                        if (this.status === 'error' && currentBlock) {
                            await this.showCurrentBlock(workspace, currentBlock)
                            this.highlightBlock(workspace, currentBlock)
                        }

                        return
                    }
                    if (currentBlock && (!resp['commands'] || !resp['commands'].length)) {  // todo зачем?
                        this.context.current_block = currentBlock
                        this.highlightBlock(workspace, currentBlock)
                        break
                    }
                }
            } catch (e) {
                this.status = 'error'
                this.result = new ExtException({parent: e, message: 'Ошибка редактора'})
            }
            this.debugProcessed = false;
        },
        _executeCommands: async function (commands) {
            if (!commands || !commands.length)
                return
            let requestedWorkspaces = []
            this.context.commands_result = []
            let commandsResult
            while (commands.length) {
                try {
                    commandsResult = await this.commandConnector.commands(commands)
                } catch (err) {
                    console.log('err com')
                    throw new ExtException({parent: err, message: 'Ошибка выполнения команды'})
                }
                try {
                    let repeat_commands = []
                    let new_workspaces = []
                    let new_commands = []
                    for (let i = 0; i < commandsResult.length; i++) {
                        let result = commandsResult[i]

                        // нас интересую только команды для которых не нашлось иники
                        if (result['status'] !== 'error' || result['result']['message'] !== 'Workspace not found') {
                            this.context.commands_result.push(result)
                            continue
                        }

                        let workspace_name = result['result']['detail']
                        if (new_workspaces.indexOf(workspace_name) >= 0) { // такую инишку уже просили в этой итерации
                            repeat_commands.push(result['uuid'])
                            continue
                        } else {
                            if (requestedWorkspaces.indexOf(workspace_name) < 0) { // такую инишку ещё не просили
                                requestedWorkspaces.push(workspace_name)
                            } else {  // если её просят не первый раз, значит инишки не кэшируются
                                // todo добавить отдельное исключение если часто будут проблемы
                                this.context.commands_result.push(result)
                                continue
                            }
                        }
                        let workspaceXml = await this.request_workspace(workspace_name)
                        if (!workspaceXml) { // если нет такой инишки, значит возвращаем на ошибку наверх
                            this.context.commands_result.push(result)
                            continue
                        }
                        new_commands.push({
                            name: 'update_ini',
                            params: {
                                name: workspace_name,
                                value: workspaceXml
                            }
                        })
                        new_workspaces.push(workspace_name)
                        repeat_commands.push(result['uuid'])

                    }
                    for (let j = 0; j < commands.length; j++) {
                        if (repeat_commands.indexOf(commands[j]['uuid']) >= 0) {
                            new_commands.push(commands[j])
                        }
                    }
                    commands = new_commands
                } catch (err) {
                    console.log('err com')
                    throw new ExtException({parent: err})
                }
            }
        },
        showBreakpoints() {
            if (!objHasOwnProperty(this.context.breakpoints, this.settings.Algorithm)) {
                return
            }
            let breakpoints = this.context.breakpoints[this.settings.Algorithm]
            breakpoints.forEach((item) => {
                this.markBreakpointBlock(item)
            })
        },
        setBreakpoint: function (remove) {
            if (!objHasOwnProperty(this.context.breakpoints, this.settings.Algorithm)) {
                this.$set(this.context.breakpoints, this.settings.Algorithm, [])
            }
            let breakpoints = this.context.breakpoints[this.settings.Algorithm]
            let index = breakpoints.indexOf(this.selectedBlock)
            if (remove) {
                if (index >= 0) {
                    let selectedBlockNode = this.findBlockNodeById(this.selectedBlock)
                    selectedBlockNode.classList.remove('breakpoint')
                    breakpoints.splice(index, 1)
                }
            } else {
                if (breakpoints.indexOf(this.selectedBlock) < 1) {
                    this.markBreakpointBlock(this.selectedBlock)
                    breakpoints.push(this.selectedBlock)
                }
            }
        },
        markBreakpointBlock: function (block_id) {
            let selectedBlockNode = this.findBlockNodeById(block_id)
            selectedBlockNode.classList.add('breakpoint')
        }
    },

}
